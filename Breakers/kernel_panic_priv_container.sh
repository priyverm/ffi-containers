#!/bin/bash
#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream8-stress &
#Creates the privilege container
echo "Create privilege container"
podman run -d --name confusion  -v /:/host --privileged -ti fedora chroot /host
sleep 5
#Performance.sh here starts collecting metrics in vm
./performance.sh &
#Create Kernel Panic on confusion container
echo "Kernel panic on confusion"
podman exec -it confusion sh -c "echo 1 > /proc/sys/kernel/sysrq" &
podman exec -it confusion sh -c "echo "c" > /proc/sysrq-trigger" &
echo "Run some operation in Orderly"
podman exec -it orderly sh -c "stress-ng --matrix 1 -t 50s" # stress-ng matrix exercises the CPU floating point operations as well as memory and processor data cache #
#Remove the containers
sleep 5
./cleanup.sh
