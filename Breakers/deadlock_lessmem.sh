#!/bin/bash
#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream8-stress
#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d --name confusion --oom-score-adj=-1000  --oom-kill-disable=true -m 512m stream8-stress
sleep 5
#Run stress-ng in both containers
echo "Run some interferences"
podman exec -it confusion /usr/local/bin/forkbomb.sh
#Giving some time for forkbomb
echo "now wait for 60 seconds"
sleep 60
podman stats -a --no-stream
sleep 2
podman stats -a --no-stream
#Start monitoring Orderly container performace while confusion is non-responsive
podman exec -it orderly sh -c  watch-it &
#confirming forkbomb or deadlock situation in confusion by sending another command for operation.
podman exec -it confusion sh -c "stress-ng --vecmath 1 -t 50s" &
# stress-ng matrix exercises the CPU floating point operations as well as memory and processor data cache #
num1=$(podman exec -it orderly sh -c "stress-ng --matrix 0 -t 50s  &>> testlog && cat testlog | tail -1 | awk '{print $NF}'" &>> testlog && cat testlog | tail -1 | awk '{print $NF}'| sed 's/[s]//g')
echo -e "\e[1;35m Time taken by Orderly to perform operation is $num1 \e[0m"
sleep 1
num2=52.5
if awk "BEGIN {exit !($num1 >= $num2)}"; then
    echo  -e "\e[1;31m Interference is present \e[0m"
    exit 1
else
    echo -e "\e[1;32m Interference is not present \e[0m"
fi
sleep 1
#Remove the containers
sleep 5
./cleanup.sh
