#!/bin/bash
echo "*****************************************ACTIVE CONTAINERS*****************************************"
echo ""
podman ps --all
echo ""
echo "*****************************************CPU UTILIZATION*******************************************"
mpstat -P ALL 1 45 & # All CPUs output five groups one second apart
num3=$(mpstat -P ALL 1 45 | grep 'Average' |tail -1 | awk '{print $9}')
echo ""
echo "*****************************************MEMORY ALLOCATION**********************************************"
cat /proc/meminfo &
echo ""
echo "*****************************************memory statistic*************************************"
vmstat -S m 1 2
echo ""
echo "*****************************************Shared Resources*************************************************"
free -t -h -w
num4=10
if awk "BEGIN {exit !($num3 >= $num4)}"; then
    echo  -e "\e[1;31m Interfernce is present - average steal is $num3 percent \e[0m"
    exit 1
else
    echo -e "\e[1;32m Average steal is $num3 percent \e[0m"
fi
