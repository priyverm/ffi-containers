!/bin/bash
#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream9-stress
#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d --name confusion --oom-score-adj=-1000  -m 512m stream9-stress
echo "watching orderly"
./performance.sh &
#Run stress-ng in both containers Exhaust memory and cpu of container with Less memory: When under memory pressure, the kernel will start writing pages out to swap. By checking which pages in a memory mapping are not resident in memory and touching them we can force them back into memory, causing the VM system to be heavily exercised. The --page-in option enables this mode for the bigheap, mmap and vm stressors. while container with default memory performs vec math operations.
echo "Run some interferences"
podman exec -it confusion sh -c "stress-ng --vm 2 --vm-bytes 2G --mmap 2 --mmap-bytes 2G –page-in --timeout 50" &
num1=$(podman exec -it orderly sh -c "stress-ng --vecmath 1 -t 50s &>> testlog && cat testlog | tail -1 | awk '{print $NF}'" &>> testlog && cat testlog | tail -1 | awk '{print $NF}'| sed 's/[s]//g')
echo -e "\e[1;35m Time taken by Orderly to perform operation is $num1 \e[0m"
sleep 1
num2=52.5
if awk "BEGIN {exit !($num1 >= $num2)}"; then
    echo  -e "\e[1;31m Interfernce is present \e[0m"
    exit 1
else
    echo -e "\e[1;32m Interference is not present \e[0m"
fi
#Remove the containers
sleep 5
./cleanup.sh
