#!/bin/bash
num4=5
echo -e "\e[1;35m ACTIVE CONTAINERS \e[0m"
echo ""
podman ps --all
echo ""
echo -e "\e[1;35m CPU UTILIZATION \e[0m"
mpstat -P ALL 1 30 & # All CPUs output five groups one second apart
iowait=$(mpstat -P ALL 1 30 | grep Average | tail -1 | awk '{print $6}')
sleep 1
echo ""
echo -e "\e[1;35m KERNEL ERRORS- see file testlog\e[0m"
dmesg | tail &>> testlog
echo -e "\e[1;35m MEMORY STATISTICS \e[0m"
vmstat -S m 1 30 &
num3=$(vmstat -S m 1 30 |tail -1 | awk '{print $17}')
sar -B 1 30 &
mempg=$(sar -B 1 30 |grep Average | tail -1 |awk '{print $8}')
echo "Number of pages the system has reclaimed from cache (pagecache and swapcache) per second to satisfy its memory demands is $mempg"
echo ""
if awk "BEGIN {exit !($num3 >= $num4)}"; then
    echo  -e "\e[1;31m Interference is present - Average  CPU steal is $num3 percent \e[0m"
    exit 1
else
    echo -e "\e[1;32m No Interference Present - Average CPU steal is $num3 percent \e[0m"
fi
echo ""
if awk "BEGIN {exit !($mempg >= $num4)}"; then
    echo  -e "\e[1;31m Interfernce is present - Average  Page steal is $mempg percent \e[0m"
    exit 1
else
    echo -e "\e[1;32m No Interference Present - Average Page steal is $mempg percent \e[0m"
fi
echo ""
if awk "BEGIN {exit !($iowait >= $num4)}"; then
    echo  -e "\e[1;31m Interfernce is present - Average iowait is $iowait percent \e[0m"
    exit 1
else
    echo -e "\e[1;32m No Interference Present - Average iowait is $iowait percent \e[0m"
fi

