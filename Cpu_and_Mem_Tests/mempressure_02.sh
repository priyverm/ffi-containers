#!/bin/bash
num2=52.5
#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream8-stress
#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d --name confusion --oom-score-adj=-1000  -m 512m stream8-stress
#Performance.sh here starts collecting metrics in vm
./performance.sh &
#Run stress-ng in both containers Forcing memory pressure (The --brk (expand heap break point), --stack (expand stack), --bigheap stressors try to rapidly consume memory. The kernel will eventually kill these using the Out Of Memory (OOM) killer, however, stress-ng will respawn the processes to keep the kernel busy. On a system with swap enabled the swap device will be heavily exercised.kept swap = 0)
echo "Run some interferences"
podman exec -it confusion sh -c "stress-ng --brk 2 --stack 2 --bigheap 2 -t 50s" &
num1=$(podman exec -it orderly sh -c "stress-ng --vecmath 1 -t 50s &>> testlog && cat testlog | tail -1 | awk '{print $NF}'" &>> testlog && cat testlog | tail -1 | awk '{print $NF}'| sed 's/[s]//g')
echo -e "\e[1;35m Time taken by Orderly to perform operation is $num1 \e[0m"
sleep 1
if awk "BEGIN {exit !($num1 >= $num2)}"; then
    echo  -e "\e[1;31m Interfernce is present \e[0m"
    exit 1
else
    echo -e "\e[1;32m Interference is not present \e[0m"
fi
#Remove the containers
sleep 5
./cleanup.sh
