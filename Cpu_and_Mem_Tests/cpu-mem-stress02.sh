#!/bin/bash
#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream9-stress
#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d --name confusion --oom-score-adj=-1000  -m 512m stream9-stress
#Performance.sh here starts collecting metrics in vm
./performance.sh &
#Run stress-ng in both containers Exhaust memory and cpu of container with Less memory while container with default memory performs vec math operations.
echo "Run some interferences"
podman exec -it confusion sh -c "stress-ng --cpu 4 --vm 2 --vm-bytes 7G --timeout 50s" & 
num1=$(podman exec -it orderly sh -c "stress-ng --vecmath 1 -t 50s &>> testlog && cat testlog | tail -1 | awk '{print $NF}'" &>> testlog && cat testlog | tail -1 | awk '{print $NF}'| sed 's/[s]//g')
echo -e "\e[1;35m Time taken by Orderly to perform operation is $num1 \e[0m"
sleep 1
num2=52.5
if awk "BEGIN {exit !($num1 >= $num2)}"; then
    echo  -e "\e[1;31m Interfernce is present \e[0m"
    exit 1
else
    echo -e "\e[1;32m Interference is not present \e[0m"
fi
sleep 1
#Remove the containers
sleep 5
./cleanup.sh
