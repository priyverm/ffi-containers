#!/bin/bash
num2=94.5
#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream9-stress
#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d --name confusion --oom-score-adj=-1000  -m 512m stream9-stress
#Performance.sh here starts collecting metrics
./performance.sh &
#Run stress-ng in both containers
echo "Run some interferences"
podman exec -it confusion sh -c "stress-ng --tmpfs 50 --timeout 90" &
num1=$(podman exec -it orderly sh -c "stress-ng --matrix 0 --timeout 90 &>> testlog && cat testlog | tail -1 | awk '{print $NF}'" &>> testlog && cat testlog | tail -1 | awk '{print $8}'| sed 's/[s]//g')
echo -e "\e[1;36m Time taken by Orderly to perform operation is $num1 \e[0m"
if awk "BEGIN {exit !($num1 >= $num2)}"; then
    echo  -e "\e[1;31m Interference is present- significant delay in operation by orderly container \e[0m"
    exit 1
else
    echo -e "\e[1;32m Interference is not present- no - significant delay in operation by orderly container \e[0m"
fi
#Remove the containers
sleep 10
./cleanup.sh
