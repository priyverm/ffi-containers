#Here $timper is allowed percentage delay and $ploss is allowed packet loss
timper=5
ploss=0
echo " "
echo "**********Creating Containers**********"
echo " "
podman run -d --name orderly stream8-stress
podman run -d --name confusion stream8-stress
echo " "
podman ps --all
echo " "
echo "**********Getting IP of Orderly*********"
echo " "
podman inspect orderly | grep IPAddress
var=$(podman inspect orderly | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
echo " "
echo "**********Getting IP of Confusion**********"
echo " "
podman inspect confusion | grep IPAddress
var2=$(podman inspect confusion | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
echo " "
echo "*****Orderly pings google.com 10 times*****"
echo " "
var3=$(podman exec -it orderly sh -c "ping -c 10 google.com" | grep packet| tail -1 | awk '{print $NF}' | sed 's/[ms]//g')
sleep 2
echo -e "\e[1;35m Time taken to ping in ideal condition is $var3 ms \e[0m"
echo "*****Confusion container sends large unstoppable packets to orderly, while orderly pings google.com 10 times*****"
echo " "
podman exec -it confusion sh -c "ping -s 7000 $var" &
sleep 5
var4=$(podman exec -it orderly sh -c "ping -c 10 google.com" | grep packet | tail -1 | awk '{print $NF}'| sed 's/[ms]//g')
echo ""
sleep 2
echo  -e "\e[1;35m Time taken to ping in while in a heavy traffic is $var4 ms \e[0m"
var5=$(awk -v t1="$var3" -v t2="$var4" 'BEGIN{printf "%.0f", (t2-t1)/t1 * 100}')
sleep 1
echo ""
echo -e "\e[1;32m Percentage increase in time taken is $var5 percent \e[0m"
echo ""
if [[ $var5 -gt $timper ]]
then
  echo -e "\e[1;31m Interference is present - Delay in pinging google.com when under stress \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No Delay in pinging google.com under stress condition created by Confusion Container \e[0m"
fi
sleep 1
echo "**********Lets monitor the packets**********"
./watch-it.sh &
var6=$(podman exec -it orderly sh -c "ping -c 20 google.com"  | grep packet | tail -1 | awk '{print $4}')
echo -e "\e[1;32m packet loss is $var6 percent  \e[0m"
if [[ $var6 -gt $ploss ]]
then
  echo -e "\e[1;31m Interfernce is present - presence of packet loss \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No packet loss \e[0m"
fi
./cleanup.sh &
